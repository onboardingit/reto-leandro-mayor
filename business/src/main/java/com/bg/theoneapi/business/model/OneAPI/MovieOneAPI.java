package com.bg.theoneapi.business.model.OneAPI;

import com.bg.theoneapi.business.model.Movie;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

/**
 * Movie
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T18:37:24.947988-05:00[America/Panama]")

public class MovieOneAPI {

  @JsonProperty("_id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("runtimeInMinutes")
  private Long runtimeInMinutes = null;

  @JsonProperty("budgetInMillions")
  private Long budgetInMillions = null;

  @JsonProperty("boxOfficeRevenueInMillions")
  private Long boxOfficeRevenueInMillions = null;

  @JsonProperty("academyAwardNominations")
  private Long academyAwardNominations = null;

  @JsonProperty("academyAwardWins")
  private Long academyAwardWins = null;

  @JsonProperty("rottenTomatesScore")
  private Long score = null;

  public MovieOneAPI id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   *
   * @return id
   **/
  @Schema(description = "")

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public MovieOneAPI name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   *
   * @return name
   **/
  @Schema(description = "")

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public MovieOneAPI runtimeInMinutes(Long runtimeInMinutes) {
    this.runtimeInMinutes = runtimeInMinutes;
    return this;
  }

  /**
   * Get runtimeInMinutes
   *
   * @return runtimeInMinutes
   **/
  @Schema(description = "")

  public Long getRuntimeInMinutes() {
    return runtimeInMinutes;
  }

  public void setRuntimeInMinutes(Long runtimeInMinutes) {
    this.runtimeInMinutes = runtimeInMinutes;
  }

  public MovieOneAPI budgetInMillions(Long budgetInMillions) {
    this.budgetInMillions = budgetInMillions;
    return this;
  }

  public Movie toMovie() {
    Movie movie = new Movie();
    movie.setId(this.id);
    movie.setName(this.name);
    movie.setRuntimeInMinutes(this.runtimeInMinutes);
    movie.setBudgetInMillions(this.budgetInMillions);
    movie.setBoxOfficeRevenueInMillions(this.boxOfficeRevenueInMillions);
    movie.setAcademyAwardNominations(this.academyAwardNominations);
    movie.setAcademyAwardWins(this.academyAwardWins);
    movie.setScore(this.score);
    return movie;
  }
}
