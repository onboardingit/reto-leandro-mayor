package com.bg.theoneapi.business.model.OneAPI;

import com.bg.theoneapi.business.model.Movie;
import com.bg.theoneapi.business.model.Quote;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Objects;
import org.springframework.validation.annotation.Validated;

/**
 * Quote
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-12T12:40:48.202624-05:00[America/Panama]")

public class QuoteOneAPI {

  @JsonProperty("_id")
  private String id = null;

  @JsonProperty("dialog")
  private String dialog = null;

  @JsonProperty("movie")
  private String movieName = null;

  @JsonProperty("character")
  private String characterName = null;

  public QuoteOneAPI id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   *
   * @return id
   **/
  @Schema(description = "")

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public QuoteOneAPI dialog(String dialog) {
    this.dialog = dialog;
    return this;
  }

  /**
   * Get dialog
   *
   * @return dialog
   **/
  @Schema(description = "")

  public String getDialog() {
    return dialog;
  }

  public void setDialog(String dialog) {
    this.dialog = dialog;
  }

  public QuoteOneAPI movieName(String movieName) {
    this.movieName = movieName;
    return this;
  }

  /**
   * Get movieName
   *
   * @return movieName
   **/
  @Schema(description = "")

  public String getMovieName() {
    return movieName;
  }

  public void setMovieName(String movieName) {
    this.movieName = movieName;
  }

  public QuoteOneAPI characterName(String characterName) {
    this.characterName = characterName;
    return this;
  }

  /**
   * Get characterName
   *
   * @return characterName
   **/
  @Schema(description = "")

  public String getCharacterName() {
    return characterName;
  }

  public void setCharacterName(String characterName) {
    this.characterName = characterName;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    QuoteOneAPI quoteOneAPI = (QuoteOneAPI) o;
    return Objects.equals(this.id, quoteOneAPI.id) &&
        Objects.equals(this.dialog, quoteOneAPI.dialog) &&
        Objects.equals(this.movieName, quoteOneAPI.movieName) &&
        Objects.equals(this.characterName, quoteOneAPI.characterName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dialog, movieName, characterName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Quote {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    dialog: ").append(toIndentedString(dialog)).append("\n");
    sb.append("    movieName: ").append(toIndentedString(movieName)).append("\n");
    sb.append("    characterName: ").append(toIndentedString(characterName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first
   * line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


  public Quote toQuote() {
    Quote quote = new Quote();
    quote.setId(this.id);
    quote.setDialog(this.dialog);
    quote.setMovieName(this.movieName);
    quote.characterName(this.characterName);
    return quote;
  }
}
