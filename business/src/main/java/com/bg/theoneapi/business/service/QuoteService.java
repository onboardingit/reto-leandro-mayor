package com.bg.theoneapi.business.service;

import com.bg.theoneapi.business.config.GlobalConfig;
import com.bg.theoneapi.business.model.Book;
import com.bg.theoneapi.business.model.Books;
import com.bg.theoneapi.business.model.Chapters;
import com.bg.theoneapi.business.model.Character;
import com.bg.theoneapi.business.model.Characters;
import com.bg.theoneapi.business.model.Movie;
import com.bg.theoneapi.business.model.Movies;
import com.bg.theoneapi.business.model.OneAPI.BooksOneAPI;
import com.bg.theoneapi.business.model.OneAPI.ChaptersOneAPI;
import com.bg.theoneapi.business.model.OneAPI.QuotesOneAPI;
import com.bg.theoneapi.business.model.Quote;
import com.bg.theoneapi.business.model.Quotes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuoteService {

  @Autowired
  private RestService<?, QuotesOneAPI> restService;

  @Autowired
  private GlobalConfig config;

  @Autowired
  private CharacterService characterService;

  @Autowired
  private MovieService movieService;

  private final int PAGE_LIMIT = 25;

  public String getCharactersIds(Quotes quotes) {
    List<String> charsIds = new ArrayList<String>();

    for (Quote quote : quotes.getDocs()) {
      charsIds.add(quote.getCharacterName());
    }

    return StringUtils.join(charsIds, ',');
  }

  public String getMoviesIds(Quotes quotes) {
    List<String> moviesIds = new ArrayList<String>();

    for (Quote quote : quotes.getDocs()) {
      moviesIds.add(quote.getMovieName());
    }

    return StringUtils.join(moviesIds, ',');
  }

  public Quotes getQuotes(String sort, Long limit, Long page) {

    Map<String, String> params = new HashMap();

    if (sort != null) {
      params.put("sort", sort);
    }

    if (limit != null) {
      params.put("limit", limit.toString());
    } else {
      params.put("limit", String.valueOf(PAGE_LIMIT));
    }

    if (page != null) {
      params.put("page", page.toString());
    }

    Quotes quotes = this.restService
        .getRequest(config.getQuotesEndpoint(), params, QuotesOneAPI.class,
            this.config.getOneApiAccessToken()).toQuotesDTO();

    String charactersIds = this.getCharactersIds(quotes);
    Characters characters = characterService.getCharacters(charactersIds);

    String moviesIds = this.getMoviesIds(quotes);
    Movies movies = movieService.getMovies(moviesIds);

    for (Quote quote : quotes.getDocs()) {
      Character character = characterService
          .findCharacter(quote.getCharacterName(), characters.getDocs());
      Movie movie = movieService.findMovie(quote.getMovieName(), movies.getDocs());

      if (character != null) {
        quote.setCharacterName(character.getName());
      }

      if (movie != null) {
        quote.setMovieName(movie.getName());
      }
    }

    return quotes;
  }
}