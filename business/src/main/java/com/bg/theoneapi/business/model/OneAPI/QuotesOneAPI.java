package com.bg.theoneapi.business.model.OneAPI;

import com.bg.theoneapi.business.model.Movie;
import com.bg.theoneapi.business.model.Movies;
import com.bg.theoneapi.business.model.Quote;
import com.bg.theoneapi.business.model.Quotes;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;

/**
 * Quotes
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-12T12:40:48.202624-05:00[America/Panama]")


public class QuotesOneAPI {
  @JsonProperty("docs")
  @Valid
  private List<QuoteOneAPI> docs = null;

  @JsonProperty("page")
  private Long page = null;

  @JsonProperty("pages")
  private Long pages = null;

  public QuotesOneAPI docs(List<QuoteOneAPI> docs) {
    this.docs = docs;
    return this;
  }

  public QuotesOneAPI addDocsItem(QuoteOneAPI docsItem) {
    if (this.docs == null) {
      this.docs = new ArrayList<QuoteOneAPI>();
    }
    this.docs.add(docsItem);
    return this;
  }

  /**
   * Get docs
   * @return docs
   **/
  @Schema(description = "")
      @Valid
    public List<QuoteOneAPI> getDocs() {
    return docs;
  }

  public void setDocs(List<QuoteOneAPI> docs) {
    this.docs = docs;
  }

  public QuotesOneAPI page(Long page) {
    this.page = page;
    return this;
  }

  /**
   * Get page
   * @return page
   **/
  @Schema(description = "")
  
    public Long getPage() {
    return page;
  }

  public void setPage(Long page) {
    this.page = page;
  }

  public QuotesOneAPI pages(Long pages) {
    this.pages = pages;
    return this;
  }

  /**
   * Get pages
   * @return pages
   **/
  @Schema(description = "")
  
    public Long getPages() {
    return pages;
  }

  public void setPages(Long pages) {
    this.pages = pages;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    QuotesOneAPI quotesOneAPI = (QuotesOneAPI) o;
    return Objects.equals(this.docs, quotesOneAPI.docs) &&
        Objects.equals(this.page, quotesOneAPI.page) &&
        Objects.equals(this.pages, quotesOneAPI.pages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docs, page, pages);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Quotes {\n");
    
    sb.append("    docs: ").append(toIndentedString(docs)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("    pages: ").append(toIndentedString(pages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public Quotes toQuotesDTO() {
    Quotes quotes = new Quotes();
    List<Quote> quotesList = new ArrayList();
    this.docs.forEach(quote -> {
      quotesList.add(quote.toQuote());
    });
    quotes.setDocs(quotesList);
    quotes.setPage(this.page);
    quotes.setPages(this.pages);
    return quotes;
  }
}
