package com.bg.theoneapi.backend.exception;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

public class ExceptionInfo {

  @JsonProperty("message")
  @Getter
  private String message;

  @JsonProperty("timestamp")
  @Getter
  private String timestamp;

  @JsonProperty("path")
  @Getter
  private String uriRequested;

  public ExceptionInfo(String message, String uriRequested) {
    this.message = message;
    this.uriRequested = uriRequested;
    this.timestamp = this.dateFormat();
  }

  public String dateFormat() {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = new Date();
    return dateFormat.format(date);
  }
}