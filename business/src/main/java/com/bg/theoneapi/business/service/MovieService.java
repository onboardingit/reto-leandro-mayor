package com.bg.theoneapi.business.service;

import com.bg.theoneapi.business.config.GlobalConfig;
import com.bg.theoneapi.business.model.Character;
import com.bg.theoneapi.business.model.Movie;
import com.bg.theoneapi.business.model.Movies;
import com.bg.theoneapi.business.model.OneAPI.MoviesOneAPI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieService {

  @Autowired
  private RestService<?, MoviesOneAPI> restService;

  @Autowired
  private GlobalConfig config;

  public Movies getMovies() {
    Map<String, String> params = new HashMap();
    params.put("sort", "name:asc");

    return this.restService
        .getRequest(config.getMoviesEndpoint(), params, MoviesOneAPI.class, config.getOneApiAccessToken())
        .toMoviesDTO();
  }

  public Movies getMovies(String ids) {
    Map<String, String> params = new HashMap();

    if (ids != null) {
      params.put("_id", ids);
    }

    return this.restService
        .getRequest(config.getMoviesEndpoint(), params, MoviesOneAPI.class, config.getOneApiAccessToken())
        .toMoviesDTO();
  }

  public Movie findMovie(String id, List<Movie> movies) {
    for (Movie movie : movies) {
      if (movie.getId().equals(id)) {
        return movie;
      }
    }
    return null;
  }
}
