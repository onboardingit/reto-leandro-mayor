package com.bg.theoneapi.backend;

import com.bg.theoneapi.backend.controller.BackendController;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BackendApplicationTests {

  @Test
  void contextLoads() {
  }

  @Test
  public void testBackendController() {
    BackendController backendController = new BackendController();
    ResponseEntity<String> result = backendController.healthCheck();
    assertEquals(result.getStatusCodeValue(), 200);
    assertEquals(result.getBody(), "OK");

  }

}
