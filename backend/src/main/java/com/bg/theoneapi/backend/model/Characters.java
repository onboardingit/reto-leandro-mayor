package com.bg.theoneapi.backend.model;

import java.util.Objects;
import com.bg.theoneapi.backend.model.Character;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Characters
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-17T18:22:31.014443-05:00[America/Panama]")


public class Characters   {
  @JsonProperty("docs")
  @Valid
  private List<Character> docs = null;

  public Characters docs(List<Character> docs) {
    this.docs = docs;
    return this;
  }

  public Characters addDocsItem(Character docsItem) {
    if (this.docs == null) {
      this.docs = new ArrayList<Character>();
    }
    this.docs.add(docsItem);
    return this;
  }

  /**
   * Get docs
   * @return docs
   **/
  @Schema(description = "")
      @Valid
    public List<Character> getDocs() {
    return docs;
  }

  public void setDocs(List<Character> docs) {
    this.docs = docs;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Characters characters = (Characters) o;
    return Objects.equals(this.docs, characters.docs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Characters {\n");
    
    sb.append("    docs: ").append(toIndentedString(docs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
