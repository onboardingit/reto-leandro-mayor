package com.bg.theoneapi.business.service;

import com.bg.theoneapi.business.config.GlobalConfig;
import com.bg.theoneapi.business.model.Character;
import com.bg.theoneapi.business.model.Characters;
import com.bg.theoneapi.business.model.OneAPI.CharactersOneAPI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CharacterService {

  @Autowired
  private RestService<?, CharactersOneAPI> restService;

  @Autowired
  private GlobalConfig config;

  public Characters getCharacters() {
    Map<String, String> params = new HashMap();

    params.put("sort", "name:desc");
    params.put("limit", "100");

    return this.restService
        .getRequest(config.getCharactersEndpoint(), params, CharactersOneAPI.class,
            config.getOneApiAccessToken())
        .toCharactersDTO();
  }

  public Characters getCharacters(String ids) {
    Map<String, String> params = new HashMap();

    if (ids != null) {
      params.put("_id", ids);
    }

    return this.restService
        .getRequest(config.getCharactersEndpoint(), params, CharactersOneAPI.class,
            config.getOneApiAccessToken())
        .toCharactersDTO();
  }

  public Character findCharacter(String id, List<Character> characters) {
    for (Character character : characters) {
      if (character.getId().equals(id)) {
        return character;
      }
    }
    return null;
  }
}
