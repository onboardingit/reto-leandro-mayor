package com.bg.theoneapi.backend.service;

import com.bg.theoneapi.backend.config.GlobalConfig;
import com.bg.theoneapi.backend.model.Chapters;
import com.bg.theoneapi.backend.model.Quotes;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuoteService {

  @Autowired
  private RestService<?, Quotes> restService;

  @Autowired
  private GlobalConfig config;

  public Quotes getQuotes(String sort, Long page) {
    Map<String, String> params = new HashMap();

    if (sort != null) {
      params.put("sort", sort);
    }

    if (page != null) {
      params.put("page", page.toString());
    }
    
    return this.restService.getRequest(config.getBusinessQuotesUrl(), params, Quotes.class);
  }
}
