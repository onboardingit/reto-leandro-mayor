package com.bg.theoneapi.backend.service;

import com.bg.theoneapi.backend.config.GlobalConfig;
import com.bg.theoneapi.backend.model.Chapters;
import com.bg.theoneapi.backend.model.Characters;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChapterService {

  @Autowired
  private RestService<?, Chapters> restService;

  @Autowired
  private GlobalConfig config;

  public Chapters getChapters(String sort) {
    Map<String, String> params = new HashMap();

    if (sort != null) {
      params.put("sort", sort);
    }

    return this.restService.getRequest(config.getBusinessChaptersUrl(), params, Chapters.class);
  }
}
