package com.bg.theoneapi.business.model;

import java.util.Objects;
import com.bg.theoneapi.business.model.Chapter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Chapters
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-17T18:23:10.149596-05:00[America/Panama]")


public class Chapters   {
  @JsonProperty("docs")
  @Valid
  private List<Chapter> docs = null;

  public Chapters docs(List<Chapter> docs) {
    this.docs = docs;
    return this;
  }

  public Chapters addDocsItem(Chapter docsItem) {
    if (this.docs == null) {
      this.docs = new ArrayList<Chapter>();
    }
    this.docs.add(docsItem);
    return this;
  }

  /**
   * Get docs
   * @return docs
   **/
  @Schema(description = "")
      @Valid
    public List<Chapter> getDocs() {
    return docs;
  }

  public void setDocs(List<Chapter> docs) {
    this.docs = docs;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Chapters chapters = (Chapters) o;
    return Objects.equals(this.docs, chapters.docs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Chapters {\n");
    
    sb.append("    docs: ").append(toIndentedString(docs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
