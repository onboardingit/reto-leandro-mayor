package com.bg.theoneapi.backend.exception;

import io.swagger.codegen.v3.service.exception.NotFoundException;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class NotFoundResourceExceptionResolver {

  @ExceptionHandler(NoHandlerFoundException.class)
  public ResponseEntity<ExceptionInfo> handleException(final Exception exception,
      HttpServletRequest request) {
    ResponseEntity<ExceptionInfo> errorInfoResponseEntity = new ResponseEntity<>(
        new ExceptionInfo("Resource " + request.getRequestURI() + " not found",
            request.getRequestURI()),
        HttpStatus.NOT_FOUND);
    return errorInfoResponseEntity;
  }
}