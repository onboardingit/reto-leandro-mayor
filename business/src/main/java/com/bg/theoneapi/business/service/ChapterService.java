package com.bg.theoneapi.business.service;

import com.bg.theoneapi.business.config.GlobalConfig;
import com.bg.theoneapi.business.model.Book;
import com.bg.theoneapi.business.model.Books;
import com.bg.theoneapi.business.model.Chapter;
import com.bg.theoneapi.business.model.Chapters;
import com.bg.theoneapi.business.model.OneAPI.ChaptersOneAPI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChapterService {

  @Autowired
  private RestService<?, ChaptersOneAPI> restService;

  @Autowired
  private GlobalConfig config;

  @Autowired
  private BookService bookService;

  public Chapters getChapters(String sort) {

    Map<String, String> params = new HashMap();

    if (sort != null) {
      params.put("sort", sort);
    }

    Chapters chapters = restService
        .getRequest(config.getChaptersEndpoint(), params, ChaptersOneAPI.class,
            config.getOneApiAccessToken()).toChaptersDTO();

    Books books = bookService.getBooks(this.config.getBooksEndpoint());

    for (Chapter chapter : chapters.getDocs()) {
      Book book = bookService.findBook(chapter.getBookName(), books.getDocs());
      if (book != null) {
        chapter.setBookName(book.getName());
      }
    }

    return chapters;
  }
}
