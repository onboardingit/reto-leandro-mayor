package com.bg.theoneapi.backend.exception;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestControllerAdvice
public class GlobalExceptionResolver {

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ExceptionInfo> handleException(final Exception exception,
      HttpServletRequest request) {
    ResponseEntity<ExceptionInfo> errorInfoResponseEntity = new ResponseEntity<>(
        new ExceptionInfo(exception.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    return errorInfoResponseEntity;
  }
}