package com.bg.theoneapi.backend.service;

import com.bg.theoneapi.backend.config.GlobalConfig;
import com.bg.theoneapi.backend.model.Movies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MovieService {

  @Autowired
  private RestService<?, Movies> restService;

  @Autowired
  private GlobalConfig config;

  public Movies getMovies() {
    return this.restService.getRequest(config.getBusinessMoviesUrl(), null, Movies.class);
  }
}
