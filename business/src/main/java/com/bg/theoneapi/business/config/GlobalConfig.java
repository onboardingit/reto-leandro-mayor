package com.bg.theoneapi.business.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import lombok.Getter;
import org.springframework.web.client.RestTemplate;

@Component
public class GlobalConfig {

  @Value("${oneapi-access-token}")
  @Getter
  private String oneApiAccessToken;

  @Value("${oneapi-endpoint-movies}")
  @Getter
  private String moviesEndpoint;

  @Value("${oneapi-endpoint-characters}")
  @Getter
  private String charactersEndpoint;

  @Value("${oneapi-endpoint-books}")
  @Getter
  private String booksEndpoint;

  @Value("${oneapi-endpoint-chapters}")
  @Getter
  private String chaptersEndpoint;

  @Value("${oneapi-endpoint-quotes}")
  @Getter
  private String quotesEndpoint;
}
