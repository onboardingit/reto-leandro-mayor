package com.bg.theoneapi.backend.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GlobalConfig {

  @Value("${services.business.health-check}")
  @Getter
  private String businessHealthCheckUrl;

  @Value("${services.business.movies}")
  @Getter
  private String businessMoviesUrl;

  @Value("${services.business.characters}")
  @Getter
  private String businessCharactersUrl;

  @Value("${services.business.chapters}")
  @Getter
  private String businessChaptersUrl;

  @Value("${services.business.quotes}")
  @Getter
  private String businessQuotesUrl;
}
