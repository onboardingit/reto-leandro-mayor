package com.bg.theoneapi.business.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class RestService<I extends Object, O extends Object> {

  private final RestTemplate restTemplate;
  private final String AUTH_HEADER;

  @Autowired
  public RestService(RestTemplateBuilder builder) {
    this.restTemplate = builder.build();
    AUTH_HEADER = "Authorization";
  }

  public O getRequest(String url, Class<O> _class) {
    return this.getRequest(url, _class, null);
  }

  public O getRequest(String url, Class<O> _class, String authToken) {
    RequestParameters<I> parameters = new RequestParameters()
        .setHeader(AUTH_HEADER, authToken)
        .setMethod(HttpMethod.GET)
        .setUrl(url);
    return this.executeRequest(parameters, _class);
  }

  public O getRequest(String url, Map<String, String> params, Class<O> _class) {
    return this.getRequest(url, params, _class, null);
  }

  public O getRequest(String url, Map<String, String> params, Class<O> _class, String authToken) {
    RequestParameters<I> parameters = new RequestParameters()
        .setHeader(AUTH_HEADER, authToken)
        .setMethod(HttpMethod.GET)
        .setQueryParams(params)
        .setUrl(url);

    return this.executeRequest(parameters, _class);
  }

  public O executeRequest(RequestParameters<I> parameters, Class<O> _class) {
    HttpHeaders headers = new HttpHeaders();
    for (Map.Entry<String, String> entry : parameters.getHeaders().entrySet()) {
      headers.add(entry.getKey(), entry.getValue());
    }

    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(parameters.getUrl());
    if (!parameters.getQueryParams().isEmpty()) {
      parameters.getQueryParams().entrySet().forEach(entry -> {
        uriBuilder.queryParam(entry.getKey(), entry.getValue());
      });
    }

    uriBuilder.build();

    HttpEntity<I> entity = new HttpEntity<>(parameters.getBody(), headers);
    return this.restTemplate
        .exchange(uriBuilder.toUriString(), parameters.getHttpMethod(), entity, _class).getBody();
  }
}
