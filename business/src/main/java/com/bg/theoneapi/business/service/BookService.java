package com.bg.theoneapi.business.service;

import com.bg.theoneapi.business.config.GlobalConfig;
import com.bg.theoneapi.business.model.Book;
import com.bg.theoneapi.business.model.Books;
import com.bg.theoneapi.business.model.OneAPI.BooksOneAPI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {

  @Autowired
  private RestService<?, BooksOneAPI> restService;

  @Autowired
  private GlobalConfig config;

  public Books getBooks(String bookPath) {
    return this.restService.getRequest(bookPath, null, BooksOneAPI.class, config.getOneApiAccessToken()).toBooksDTO();
  }

  public Book findBook(String id, List<Book> books) {
    for (Book book : books) {
      if (book.getId().equals(id)) {
        return book;
      }
    }
    return null;
  }

}
