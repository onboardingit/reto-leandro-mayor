package com.bg.theoneapi.business;

import com.bg.theoneapi.business.model.Movie;
import com.bg.theoneapi.business.model.Movies;
import com.bg.theoneapi.business.service.MovieService;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

@SpringBootTest
class BusinessApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	List<Movie> testGetMovies()
	{
		MovieService movieService = new MovieService();
		Movies movies = movieService.getMovies();
		System.out.println(movies);
		return movieService.getMovies().getDocs();
	}

}
