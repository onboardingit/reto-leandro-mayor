package com.bg.theoneapi.business.model.OneAPI;

import com.bg.theoneapi.business.model.Book;
import com.bg.theoneapi.business.model.Books;
import com.bg.theoneapi.business.model.Character;
import com.bg.theoneapi.business.model.Characters;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;

/**
 * Books
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T23:39:27.352815-05:00[America/Panama]")

public class BooksOneAPI {

  @JsonProperty("docs")
  @Valid
  private List<BookOneAPI> docs = null;

  public BooksOneAPI docs(List<BookOneAPI> docs) {
    this.docs = docs;
    return this;
  }

  public BooksOneAPI addDocsItem(BookOneAPI docsItem) {
    if (this.docs == null) {
      this.docs = new ArrayList<BookOneAPI>();
    }
    this.docs.add(docsItem);
    return this;
  }

  /**
   * Get docs
   *
   * @return docs
   **/
  @Schema(description = "")
  @Valid
  public List<BookOneAPI> getDocs() {
    return docs;
  }

  public void setDocs(List<BookOneAPI> docs) {
    this.docs = docs;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BooksOneAPI booksOneAPI = (BooksOneAPI) o;
    return Objects.equals(this.docs, booksOneAPI.docs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Books {\n");

    sb.append("    docs: ").append(toIndentedString(docs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first
   * line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public Books toBooksDTO() {
    Books books = new Books();
    List<Book> bookList = new ArrayList();
    this.docs.forEach(book -> {
      bookList.add(book.toBook());
    });
    books.setDocs(bookList);
    return books;
  }
}
