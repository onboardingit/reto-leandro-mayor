package com.bg.theoneapi.business.model.OneAPI;

import com.bg.theoneapi.business.model.Book;
import com.bg.theoneapi.business.model.Chapter;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Objects;
import org.springframework.validation.annotation.Validated;

/**
 * Chapter
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T23:39:27.352815-05:00[America/Panama]")


public class ChapterOneAPI {
  @JsonProperty("_id")
  private String id = null;

  @JsonProperty("chapterName")
  private String chapterName = null;

  @JsonProperty("book")
  private String bookName = null;

  public ChapterOneAPI id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ChapterOneAPI chapterName(String chapterName) {
    this.chapterName = chapterName;
    return this;
  }

  /**
   * Get chapterName
   * @return chapterName
   **/
  @Schema(description = "")
  
    public String getChapterName() {
    return chapterName;
  }

  public void setChapterName(String chapterName) {
    this.chapterName = chapterName;
  }

  public ChapterOneAPI bookName(String bookName) {
    this.bookName = bookName;
    return this;
  }

  /**
   * Get bookName
   * @return bookName
   **/
  @Schema(description = "")
  
    public String getBookName() {
    return bookName;
  }

  public void setBookName(String bookName) {
    this.bookName = bookName;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChapterOneAPI chapterOneAPI = (ChapterOneAPI) o;
    return Objects.equals(this.id, chapterOneAPI.id) &&
        Objects.equals(this.chapterName, chapterOneAPI.chapterName) &&
        Objects.equals(this.bookName, chapterOneAPI.bookName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, chapterName, bookName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Chapter {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    chapterName: ").append(toIndentedString(chapterName)).append("\n");
    sb.append("    bookName: ").append(toIndentedString(bookName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public Chapter toChapter() {
    Chapter chapter = new Chapter();
    chapter.setId(this.id);
    chapter.setChapterName(this.chapterName);
    chapter.setBookName(this.bookName);
    return chapter;
  }
}
