package com.bg.theoneapi.backend.service;

import com.bg.theoneapi.backend.config.JwtTokenUtil;
import com.bg.theoneapi.backend.model.Login;
import com.bg.theoneapi.backend.model.Token;
import com.bg.theoneapi.backend.model.jwt.JwtRequest;
import com.bg.theoneapi.backend.model.jwt.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class LoginService {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private JwtUserDetailsService userDetailsService;

  public Token createAuthenticationToken(Login login)
      throws Exception {

    authenticate(login.getUsername(), login.getPassword());

    final UserDetails userDetails = userDetailsService
        .loadUserByUsername(login.getUsername());

    Token token = new Token();
    token.setToken(jwtTokenUtil.generateToken(userDetails));

    return token;
  }

  private void authenticate(String username, String password) throws Exception {
    try {
      authenticationManager
          .authenticate(new UsernamePasswordAuthenticationToken(username, password));
    } catch (DisabledException e) {
      throw new Exception("USER_DISABLED", e);
    } catch (BadCredentialsException e) {
      throw new Exception("INVALID_CREDENTIALS", e);
    }
  }
}
