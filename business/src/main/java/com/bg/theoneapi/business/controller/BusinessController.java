package com.bg.theoneapi.business.controller;

import com.bg.theoneapi.business.config.GlobalConfig;
import com.bg.theoneapi.business.model.Chapter;
import com.bg.theoneapi.business.model.Chapters;
import com.bg.theoneapi.business.model.Characters;
import com.bg.theoneapi.business.model.Movies;
import com.bg.theoneapi.business.model.Quotes;
import com.bg.theoneapi.business.service.ChapterService;
import com.bg.theoneapi.business.service.CharacterService;
import com.bg.theoneapi.business.service.MovieService;
import com.bg.theoneapi.business.service.QuoteService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BusinessController implements BusinessApi {

  // CONFIGURATION
  @Autowired
  private GlobalConfig config;

  // SERVICES
  @Autowired
  private MovieService movieService;

  @Autowired
  private CharacterService characterService;

  @Autowired
  private ChapterService chapterService;

  @Autowired
  private QuoteService quoteService;

  // ENDPOINTS
  @GetMapping("/health-check")
  public ResponseEntity<String> healthCheck() {
    return new ResponseEntity("OK", HttpStatus.OK);
  }

  @Override
  @GetMapping("/movie")
  public ResponseEntity<Movies> getMovies(@Valid String sort) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(movieService.getMovies());
  }

  @Override
  @GetMapping("/character")
  public ResponseEntity<Characters> getCharacters(@Valid String sort) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(characterService.getCharacters());
  }

  @Override
  @GetMapping("/chapter")
  public ResponseEntity<Chapters> getChapters(@Valid String sort) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(chapterService.getChapters(sort));
  }

  @Override
  @GetMapping("/quote")
  public ResponseEntity<Quotes> getQuotes(@Valid String sort, @Valid Long limit, @Valid Long page) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(quoteService.getQuotes(sort, limit, page));
  }
}
