package com.bg.theoneapi.backend.config;


import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile(value = "dev")
class DevRunner implements CommandLineRunner {

  @Override
  public void run(String... args) throws Exception {

    System.out.println("In development");
  }
}

@Component
@Profile(value = "prod & !dev")
class ProdRunner implements CommandLineRunner {

  @Override
  public void run(String... args) throws Exception {

    System.out.println("In production");
  }
}