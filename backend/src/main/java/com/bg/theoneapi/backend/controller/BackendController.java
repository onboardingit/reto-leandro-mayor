package com.bg.theoneapi.backend.controller;

import com.bg.theoneapi.backend.model.Chapter;
import com.bg.theoneapi.backend.model.Character;
import com.bg.theoneapi.backend.model.Login;
import com.bg.theoneapi.backend.model.Movie;
import com.bg.theoneapi.backend.model.Quote;
import com.bg.theoneapi.backend.model.Token;
import com.bg.theoneapi.backend.service.ChapterService;
import com.bg.theoneapi.backend.service.CharacterService;
import com.bg.theoneapi.backend.service.LoginService;
import com.bg.theoneapi.backend.service.MovieService;
import com.bg.theoneapi.backend.service.QuoteService;
import java.util.List;
import javax.validation.Valid;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BackendController implements BackendApi {

  @Autowired
  private MovieService movieService;

  @Autowired
  private CharacterService characterService;

  @Autowired
  private ChapterService chapterService;

  @Autowired
  private QuoteService quoteService;

  @Autowired
  private LoginService loginService;

  @GetMapping("/health-check")
  public ResponseEntity<String> healthCheck() {
    return new ResponseEntity("OK", HttpStatus.OK);
  }

  @Override
  @PostMapping("/movie")
  public ResponseEntity<List<Movie>> getMovies() {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(movieService.getMovies().getDocs());
  }

  @Override
  @PostMapping("/character")
  public ResponseEntity<List<Character>> getCharacters() {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(characterService.getCharacters().getDocs());
  }

  @Override
  @PostMapping("/chapter")
  public ResponseEntity<List<Chapter>> getChapters(
      @Valid String sort) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(chapterService.getChapters(sort).getDocs());
  }

  @Override
  @PostMapping("/quote")
  public ResponseEntity<List<Quote>> getQuotes(@Valid String sort, @Valid Long page) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(quoteService.getQuotes(sort, page).getDocs());
  }

  @SneakyThrows
  @Override
  @PostMapping("/authenticate")
  public ResponseEntity<Token> login(@Valid Login body) {
    return ResponseEntity
        .status(HttpStatus.OK)
        .body(loginService.createAuthenticationToken(body));
  }
}
