package com.bg.theoneapi.backend.service;

import com.bg.theoneapi.backend.config.GlobalConfig;
import com.bg.theoneapi.backend.model.Characters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CharacterService {

  @Autowired
  private RestService<?, Characters> restService;

  @Autowired
  private GlobalConfig config;

  public Characters getCharacters() {
    return this.restService.getRequest(config.getBusinessCharactersUrl(), null, Characters.class);
  }
}
