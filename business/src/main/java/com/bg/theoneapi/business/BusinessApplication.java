package com.bg.theoneapi.business;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class BusinessApplication {

  public static void main(String[] args) {
    SpringApplication.run(BusinessApplication.class, args);
  }
}

@Component
@Profile(value = "dev")
class DevRunner implements CommandLineRunner {

  @Override
  public void run(String... args) throws Exception {

    System.out.println("In development");
  }
}

@Component
@Profile(value = "prod & !dev")
class ProdRunner implements CommandLineRunner {

  @Override
  public void run(String... args) throws Exception {

    System.out.println("In production");
  }
}
