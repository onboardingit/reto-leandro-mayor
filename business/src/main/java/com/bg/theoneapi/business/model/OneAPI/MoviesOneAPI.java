package com.bg.theoneapi.business.model.OneAPI;

import com.bg.theoneapi.business.model.Movie;
import com.bg.theoneapi.business.model.Movies;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;

/**
 * Movies
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T23:39:27.352815-05:00[America/Panama]")

public class MoviesOneAPI {

  @JsonProperty("docs")
  @Valid
  private List<MovieOneAPI> docs = null;

  public MoviesOneAPI docs(List<MovieOneAPI> docs) {
    this.docs = docs;
    return this;
  }

  public MoviesOneAPI addDocsItem(MovieOneAPI docsItem) {
    if (this.docs == null) {
      this.docs = new ArrayList<MovieOneAPI>();
    }
    this.docs.add(docsItem);
    return this;
  }

  /**
   * Get docs
   *
   * @return docs
   **/
  @Schema(description = "")
  @Valid
  public List<MovieOneAPI> getDocs() {
    return docs;
  }

  public void setDocs(List<MovieOneAPI> docs) {
    this.docs = docs;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MoviesOneAPI movies = (MoviesOneAPI) o;
    return Objects.equals(this.docs, movies.docs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Movies {\n");

    sb.append("    docs: ").append(toIndentedString(docs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first
   * line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public Movies toMoviesDTO() {
    Movies movies = new Movies();
    List<Movie> moviesList = new ArrayList();
    this.docs.forEach(movie -> {
      moviesList.add(movie.toMovie());
    });
    movies.setDocs(moviesList);
    return movies;
  }
}
